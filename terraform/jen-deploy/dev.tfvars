# Provider
azure_subscription_id = "my azure subscription id"

# Shared
pjt = "clee"
env = "dev"
svr = "jen"
reg = "jpw"
location_jpw = "japanwest"
location_jpe = "japaneast"
resource_group_name = "my-jenkins-build"
storage_account_name ="myjenkinsbuilddiag643"

# Network
vnet_100_snet_10_id= "/subscriptions/<my azure subscription id>/resourceGroups/clee-jenkins-build/providers/Microsoft.Network/virtualNetworks/clee-dev-jpw-vnet-100/subnets/vnet-100-snet-10"
vnet_100_snet_20_id= "/subscriptions/<my azure subscription id>/resourceGroups/clee-jenkins-build/providers/Microsoft.Network/virtualNetworks/clee-dev-jpw-vnet-100/subnets/vnet-100-snet-20"

# Virtual Machine
vm_size = "Standard_F1S"
vm_os_disk_size_gb = 31
vm_data_disk_size_gb = 10
vm_blob_service_endpoint ="https://myjenkinsbuilddiag643.blob.core.windows.net/"
vm_admin_username = "ubuntu"
vm_admin_password = "my password"
vm_disable_password_authentication= "false"

# blobUri or OSDiskUri
vm_os_disk_image_uri = "https://myjenkinsbuilddiag643.blob.core.windows.net/system/Microsoft.Compute/Images/vhds/jen-packer-osDisk.9ba19dfe-3288-492c-892e-bc15192935a9.vhd"
