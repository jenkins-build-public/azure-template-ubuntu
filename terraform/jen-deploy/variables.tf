#####################################
# Variable Settings
#####################################
# Provider
variable "azure_subscription_id" {}
variable "azure_client_id" {}
variable "azure_secret" {}
variable "azure_tenant_id" {}

# Shared
variable "location_jpw" {}
variable "location_jpe" {}
variable "resource_group_name" {}
variable "storage_account_name" {}
variable "pjt" {}
variable "env" {}
variable "svr" {}
variable "reg" {}
variable "private_ip_address" {}
variable "private_ip_string" {}

# Network
variable "vnet_100_snet_10_id" {}

# Virtual Machine
variable "vm_size" {}
variable "vm_os_disk_image_uri" {}
variable "vm_os_disk_size_gb" {}
variable "vm_data_disk_size_gb" {}
variable "vm_admin_username" {}
variable "vm_admin_password" {}
variable "vm_disable_password_authentication" {}
variable "vm_blob_service_endpoint" {}
