resource "azurerm_public_ip" "azr" {
  name = "${var.pjt}-${var.env}-vm-${var.svr}-${var.reg}-${var.private_ip_string}-ip"
  location = "${var.location_jpw}"
  resource_group_name = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  tags {
    Name = "${var.pjt}-${var.env}-vm-${var.svr}-${var.reg}-${var.private_ip_string}"
  }
}

resource "azurerm_network_interface" "azr" {
    name = "${var.pjt}-${var.env}-vm-${var.svr}-${var.reg}-${var.private_ip_string}-if"
    location = "${var.location_jpw}"
    resource_group_name = "${var.resource_group_name}"

    ip_configuration {
        name = "${var.pjt}-${var.env}-vm-${var.svr}-${var.reg}-${var.private_ip_string}-cf"
        subnet_id = "${var.vnet_100_snet_10_id}"
        private_ip_address_allocation = "static"
        private_ip_address = "${var.private_ip_address}"
        public_ip_address_id = "${azurerm_public_ip.azr.id}"
    }
    tags {
      Name = "${var.pjt}-${var.env}-vm-${var.svr}-${var.reg}-${var.private_ip_string}"
    }
}

resource "azurerm_virtual_machine" "azr" {
  name = "${var.pjt}-${var.env}-vm-${var.svr}-${var.reg}-${var.private_ip_string}"
  location = "${var.location_jpw}"
  resource_group_name = "${var.resource_group_name}"
  network_interface_ids = ["${azurerm_network_interface.azr.id}"]
  vm_size = "${var.vm_size}"
  storage_os_disk {
    name = "${var.pjt}-${var.env}-vm-${var.svr}-${var.reg}-osDisk-${var.private_ip_string}"
    image_uri = "${var.vm_os_disk_image_uri}"
    vhd_uri = "${var.vm_blob_service_endpoint}vhd-${var.private_ip_string}/osDisk-${var.private_ip_string}.vhd"
    create_option = "FromImage"
    caching = "ReadWrite"
    os_type = "linux"
    disk_size_gb = "${var.vm_os_disk_size_gb}"
  }
  os_profile {
    computer_name = "${var.pjt}-${var.env}-vm-${var.svr}-${var.reg}-${var.private_ip_string}"
    admin_username = "${var.vm_admin_username}"
    admin_password = "${var.vm_admin_password}"
    custom_data = "${file("./custom_data/custom_data.txt")}"
  }
  os_profile_linux_config {
    disable_password_authentication = "${var.vm_disable_password_authentication}"
  }
  tags {
    Env = "${var.env}",
    Name = "${var.pjt}-${var.env}-vm-${var.svr}-${var.reg}-${var.private_ip_string}"
  }
}
