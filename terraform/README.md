# terraform
* Packerで作成したVMイメージをAzureにデプロイするコード
* Azureのリソースグルプ、ネットワーク、セキュリティグループなどは存在しているという前提で構成

# ディレクトリ構成
* terraform/gdb-deploy  
グラフデータベースサーバ(Neo4j)をデプロイするコード

* terraform/jen-deploy  
Jenkinsサーバをデプロイするコード


# 実装

* test
```
$ terraform plan -var-file=dev.tfvars
```
* build
```
$ terraform apply -var-file=dev.tfvars
```
