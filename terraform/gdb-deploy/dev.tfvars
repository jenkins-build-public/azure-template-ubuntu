# Provider
azure_subscription_id = "my azure subscription id"

# Shared
pjt = "clee"
env = "dev"
svr = "gdb"
reg = "jpw"
location_jpw = "japanwest"
location_jpe = "japaneast"
resource_group_name = "my-jenkins-build"
storage_account_name ="myjenkinsbuilddiag643"

# Network
vnet_100_snet_10_id= "/subscriptions/<my azure subscription id>/resourceGroups/clee-jenkins-build/providers/Microsoft.Network/virtualNetworks/clee-dev-jpw-vnet-100/subnets/vnet-100-snet-10"
vnet_100_snet_20_id= "/subscriptions/<my azure subscription id>/resourceGroups/clee-jenkins-build/providers/Microsoft.Network/virtualNetworks/clee-dev-jpw-vnet-100/subnets/vnet-100-snet-20"

# Virtual Machine
vm_size = "Standard_DS1_V2"
vm_os_disk_size_gb = 31
vm_data_disk_size_gb = 10
vm_blob_service_endpoint ="https://myjenkinsbuilddiag643.blob.core.windows.net/"
vm_admin_username = "ubuntu"
vm_admin_password = "my password"
vm_disable_password_authentication= "false"

# blobUri or OSDiskUri
vm_os_disk_image_uri = "https://myjenkinsbuilddiag643.blob.core.windows.net/system/Microsoft.Compute/Images/vhds/gdb-packer-osDisk.1444b647-7d7e-436f-9e75-cd7a319016a2.vhd"
