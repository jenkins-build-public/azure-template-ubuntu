#!/bin/bash -eu

export PATH="/usr/local/bin:$PATH"
cd packer/
echo "role:${ROLE_NAME}"
pwd
rm -rf *.txt *.log

# set parameter
. /var/lib/jenkins/.ssh/.azure
WHEN=`date +"%Y%m%d-%H%M"`
RESOURCE_GROUP=clee-jenkins-build
PACKER_NAME=${ROLE_NAME}-packer-${WHEN}
LOCATION=japanwest

# packer build
/sbin/packer build -var-file=./variables.json \
-var azure_client_id=${AZURE_CLIENT_ID} \
-var azure_secret=${AZURE_SECRET} \
-var azure_tenant_id=${AZURE_TENANT_ID} \
clee-${ROLE_NAME}.json \
| tee ./packer_build.log

OSDiskUri=$(tail -15 ./packer_build.log | grep OSDiskUri: | cut -d ' ' -f2)
echo ${OSDiskUri} > ./OSDiskUri.txt
echo "os-disk-uri: ${OSDiskUri}"
echo "resource-group: ${RESOURCE_GROUP}"
echo "packer-name: ${PACKER_NAME}"
echo "location: ${LOCATION}"

az login -u ${AZURE_CLIENT_ID} --service-principal -t ${AZURE_TENANT} -p ${AZURE_SECRET}

# vm image create
az image create --name ${PACKER_NAME} \
--resource-group ${RESOURCE_GROUP} \
--source ${OSDiskUri} \
--location ${LOCATION} \
--os-type Linux
