# Jenkins
用途別のJenkinsジョブの構成ファイル

# ディレクトリ構成
* jenkins/delete-resource（Jenkinsジョブの演習的なもの）  
Azure VM関連の各種リソースを削除するジョブ構成

* jenkins/gdb-deploy-deb  
グラフデータベース(Neo4j)をデプロイするジョブ構成

* jenkins/jen-deploy-deb  
Jenkinsサーバをデプロイするジョブ構成

* jenkins/packer-build  
VMイメージを作成するジョブ構成

* jenkins/send-message  
\#slackにメッセージを送るジョブ構成（Jenkinsジョブの演習的なもの）
