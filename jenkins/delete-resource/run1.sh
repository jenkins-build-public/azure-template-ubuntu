#!/bin/bash -eu

if [ $# -ne 1 ]; then
  echo "Argument not found. you need to get a argument."
  exit 1
fi

RESOURCE_GROUP=clee-jenkins-build
STORAGE_ACCOUNT=cleejenkinsbuilddiag643
VM_NAME=${1}

. /var/lib/jenkins/.ssh/.azure
az login -u ${AZURE_CLIENT_ID} --service-principal -t ${AZURE_TENANT} -p ${AZURE_SECRET}

#VM_NAME_LIST
VM_NAME_LIST=$(az vm list -g ${RESOURCE_GROUP} --query "[].name" -o tsv)

count=0
for name in ${VM_NAME_LIST}; do
#  echo $name
  if [ ${name} = ${VM_NAME} ]; then
     count=1
     break
  fi
done

if [ ${count} -eq  0 ]; then
  echo -e "\e[31m ${VM_NAME} is not found. \e[m"
  exit 1
else
  echo -e "\e[34m ${VM_NAME} is found. \e[m"
fi

#NETWORK_IF_ID
NETWORK_IF_ID=$(az vm show -g ${RESOURCE_GROUP} -n ${VM_NAME} --query "networkProfile.networkInterfaces[].id" -o tsv)

#DATA_DISK_IDS
#DATA_DISK_IDS=$(az vm show -g ${RESOURCE_GROUP} -n ${VM_NAME} --query "storageProfile.dataDisks[].managedDisk.id" -o tsv)
#DATA_DISK_IDS=$(az vm show -g ${RESOURCE_GROUP} -n ${VM_NAME} --query "storageProfile.dataDisks[].vhd.uri" -o tsv)

#OS_DISK_ID
#OS_DISK_ID=$(az vm show -g ${RESOURCE_GROUP} -n ${VM_NAME} --query "storageProfile.osDisk.managedDisk.id" -o tsv)
OS_DISK_URI=$(az vm show -g ${RESOURCE_GROUP} -n ${VM_NAME} --query "storageProfile.osDisk.vhd.uri" -o tsv)
CONTAINER_NAME=$(az vm show -g ${RESOURCE_GROUP} -n ${VM_NAME} --query "storageProfile.osDisk.vhd.uri" -o tsv | cut -d '/' -f4)

#PUBLIC_IP_ID
PUBLIC_IP_ID=$(az network nic show --ids ${NETWORK_IF_ID} -g ${RESOURCE_GROUP} --query "ipConfigurations[].publicIpAddress.id" -o tsv)
echo -e "\e[34m resource-group-name: ${RESOURCE_GROUP} \e[m"
echo ${RESOURCE_GROUP} > ./resource_group.txt
echo -e "\e[34m storage-account: ${STORAGE_ACCOUNT} \e[m"
echo ${STORAGE_ACCOUNT} > ./storage_account.txt
echo ${VM_NAME} > ./vm_name.txt
echo -e "\e[34m network-interface-id: ${NETWORK_IF_ID} \e[m"
echo ${NETWORK_IF_ID} > ./network_interface_id.txt
#echo "data-disk-ids: ${DATA_DISK_IDS}"
#echo "os-disk-id: ${OS_DISK_ID}"
echo -e "\e[34m os-disk-uri: ${OS_DISK_URI} \e[m"
echo ${OS_DISK_URI} > ./os_disk_uri.txt
echo -e "\e[34m contanier_name: ${CONTAINER_NAME} \e[m"
echo ${CONTAINER_NAME} > ./container_name.txt
echo -e "\e[34m public-ip-id: ${PUBLIC_IP_ID} \e[m"
echo ${PUBLIC_IP_ID} > ./public_ip_id.txt
