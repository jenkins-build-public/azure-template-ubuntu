#!/bin/bash -x

if [ $# -ne 1 ]; then
  echo "Argument not found. you need to get a argument."
  exit 1
fi

RESOURCE_GROUP=$(cat ./resource_group.txt)
STORAGE_ACCOUNT=$(cat ./storage_account.txt)
VM_NAME=$(cat ./vm_name.txt)
NETWORK_IF_ID=$(cat ./network_interface_id.txt)
CONTAINER_NAME=$(cat ./container_name.txt)
PUBLIC_IP_ID=$(cat ./public_ip_id.txt)

. /var/lib/jenkins/.ssh/.azure
az login -u ${AZURE_CLIENT_ID} --service-principal -t ${AZURE_TENANT} -p ${AZURE_SECRET}

# delate vm
echo -e "\e[34m delete vm-name: ${VM_NAME} \e[34m"
az vm delete -n ${VM_NAME} -g ${RESOURCE_GROUP} -y

# delete nic
echo -e "\e[34m delete network-interface: ${NETWORK_IF_ID} \e[34m"
az network nic delete --ids ${NETWORK_IF_ID} -g ${RESOURCE_GROUP}

# delete public-ip
echo -e "\e[34m delete public-ip: ${PUBLIC_IP_ID} \e[34m"
az network public-ip delete --ids ${PUBLIC_IP_ID} -g ${RESOURCE_GROUP}

# delete container
echo -e "\e[34m delete contaner: ${CONTAINER_NAME} \e[34m"
az storage container delete -n ${CONTAINER_NAME} --account-name ${STORAGE_ACCOUNT}

exit 0

# Below ManagedDisk Only
# delete os-disk
echo -e "\e[34m delete os-disk: ${OS_DISK_ID} \e[34m"
az disk delete --ids ${OS_DISK_ID} -g ${RESOURCE_GROUP} -y

# delete data-disk
if [ "${DATA_DISK_IDS}" != "" ]; then
  for name in ${DATA_DISK_IDS}; do
      echo -e "\e[34m delete data-disk: ${name} \e[34m"
      az disk delete --ids ${name} -g ${RESOURCE_GROUP} -y
  done
fi
