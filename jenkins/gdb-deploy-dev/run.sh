#! /bin/bash -eu

# init
if [ $# -ne 1 ]; then
  echo "Argument of plan or apply is required." 1>&2
  exit 1
fi

cd terraform/gdb-deploy
rm -rf ./terraform.tfstate* *.txt

export PATH="/usr/local/bin:$PATH"
IP_STRING=`echo ${IP_ADDR} | tr '.' '-'`

# set credential
. /var/lib/jenkins/.ssh/.azure

case "${1}" in
  "plan" | "apply")
     terraform ${1} -var-file=dev.tfvars \
     -var private_ip_address=${IP_ADDR} \
     -var private_ip_string=${IP_STRING} \
     -var azure_client_id=${AZURE_CLIENT_ID} \
     -var azure_secret=${AZURE_SECRET} \
     -var azure_tenant_id=${AZURE_TENANT}

     public_ip=`terraform show | grep -e '  ip_address =' | cut -d '=' -f2 | tr -d ' '`
     echo ${public_ip} > ./public_ip.txt
     ;;
  * )
     echo "Argument of plan or apply is required."
     exit 1
     ;;
esac
