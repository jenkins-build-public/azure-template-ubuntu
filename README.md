# JenkinsによるCI/CDテンプレート(Azure版）とは

JenkinsのパイプラインジョブでAzureにVMをデプロイするテンプレートで、Packer(Ansible＋Serverspec)でVMイメージを作成し、TerraformでAzureにデプロイする構成になっています。このテンプレートは、ちょっと拡張すれば、そのまま実務に利用できるぐらい実践的な構成になっています。テンプレート利用において、特に制限はありません。ただ、技術的な中身については、クリエーションライン社及び著者が保証するものではありません。自己責任の下でご利用お願いします。

実行環境の準備など利用方法の解説は、次を参照してください。  
[www.slideshare.net/awk2561/jenkins-cicdazure-81967048](https://www.slideshare.net/awk2561/jenkins-cicdazure-81967048)


# ディレクトリ構成

* jenkins  
Jenkins Pipelineジョブのコード、Jenkinsfile、run.shなど

* packer  
Ansible+Serverspecを用いたVMイメージ作成のコード

* terraform  
Jenkinsサーバ、グラフデータベースサーバ(Noe4j)をTerraformでデプロイするコード
