#!/bin/bash
set -eu

# credential
AZURE_CLIENT_ID=<my azure client id>
AZURE_SECRET=<my azure secret key>
AZURE_TENANT=<my azure tenant id>

# get metadata
az login -u ${AZURE_CLIENT_ID} --service-principal -t ${AZURE_TENANT} -p ${AZURE_SECRET}
VM_ID=$(curl -sH Metadata:true "http://169.254.169.254/metadata/instance/compute/vmId?api-version=2017-04-02&format=text")
ROLE_NAME=$(az vm list --query "[?contains(vmId,'$VM_ID')].[tags.roleName]" --output tsv)

echo "vmId:${VM_ID}"
echo "specType:${ROLE_NAME}"

# add switch
case "${ROLE_NAME}" in
  "gdb" )
    SPEC_TYPE=${ROLE_NAME} ;;
  "jen" )
    SPEC_TYPE=${ROLE_NAME} ;;
esac

# run serverspec
export PATH=$PATH:/usr/local/bin/
gem install serverspec
gem install rake
cd /tmp/serverspec
rake spec:${SPEC_TYPE} | tee serverspec.tmp
result=`cat serverspec.tmp | grep "0 failures" | wc -l`
if [ $result -ne 1 ]; then
  echo -e "\e[31mServerspec testing is failure!!\e[m"
  exit 1
else
  echo -e "\e[32mServerspec testing is success!!\e[m"
  rm -rf /tmp/serverspec
  exit 0
fi
