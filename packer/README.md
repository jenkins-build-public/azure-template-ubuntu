# packer
* AnsibleコードでAzure上に一時的なVMを起動
* Ansibleコードを反映し、Serverspecでコードの正確性をチェック
* コードが正常の場合、Azure上にVMイメージを登録し、一時的なVM及び環境をクリナップ
* コードが正常ではないの場合、一時的なVM及び環境を削除

# OS
* Ubuntu16.04-LTS  

# ディレクトリ及びファイル構成
* packer/ansible  
Ansibleのコード

* packer/ansible/roles/base  
すべてのVMに共通する構成をコード化

* packer/ansible/roles/gdb  
グラフデータベース(Neo4j)固有の構成をコード化

* packer/ansible/roles/jen  
Jenkinsサーバの固有の構成をコード化

* packer/scripts  
VMのなかで一時的に使うスクリプト及び実行環境を削除

* packer/serverspec/{base|gdb|jen}  
ansile/roles/{base|gdb|jen}のコードをチェック

* packer/serverspec/{base|gdb|jen}  
ansile/roles/{base|gdb|jen}のコードをチェック

* packer/clee-{gdb|jen}.json  
Ansible及びServerspecの実行定義、VMイメージ作成のための一般化

* packer/variables.json  
Azure接続のための認証キーなど

# 実装

* test
```
$ packer validate -var-file=variables.json clee-${role_type}.json
```
* build
```
$ packer build -var-file=variables.json clee-${role_type}.json
```
