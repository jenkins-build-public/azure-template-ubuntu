require 'spec_helper'

# packages
%w[xfsprogs openjdk-8-jdk].each do |pkg|
  describe package(pkg) do
    it { should be_installed }
  end
end

# neo4j.list
describe file('/etc/apt/sources.list.d/neo4j.list') do
  it { should be_file }
end

# apt-key list Neo4j
describe command('apt-key list') do
  its(:stdout) { should match /Neo4j/ }
end

# neo4j
describe command('apt list neo4j') do
  its(:stdout) { should match /neo4j/ }
end
