require 'spec_helper'

# packages
%w[git jenkins apache2 openjdk-8-jdk openssl].each do |pkg|
  describe package(pkg) do
    it { should be_installed }
  end
end

# apt-key list jenkins
#describe command('apt-key list') do
#  its(:stdout) { should match /Kohsuke/ }
#end

# jenkins.list
describe file('/etc/apt/sources.list.d/jenkins.list') do
  it { should be_file }
  it { should be_mode 644 }
end


# /var/lib/jenkins/.ssh
describe file('/var/lib/jenkins/.ssh') do
  it { should be_directory }
  it { should be_owned_by 'jenkins' }
  it { should be_grouped_into 'jenkins' }
  it { should be_mode 700 }
end

# clee-jenkins-build.pem
describe file('/var/lib/jenkins/.ssh/clee-jenkins-build.pem') do
  it { should be_file }
  it { should be_owned_by 'jenkins' }
  it { should be_grouped_into 'jenkins' }
  it { should be_mode 600 }
end

# .azure
describe file('/var/lib/jenkins/.ssh/.azure') do
  it { should be_file }
  it { should be_owned_by 'jenkins' }
  it { should be_grouped_into 'jenkins' }
  it { should be_mode 600 }
end
