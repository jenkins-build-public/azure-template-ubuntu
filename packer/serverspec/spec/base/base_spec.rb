require 'spec_helper'

# grub
describe file('/etc/default/grub') do
  it { should be_file }
end

# waagent.conf
describe file('/etc/waagent.conf') do
  it { should be_file }
end

# packages
%w[libssl-dev libffi-dev python-dev build-essential ruby ruby-dev].each do |pkg|
  describe package(pkg) do
    it { should be_installed }
  end
end

# .profile
describe file('/root/.profile') do
  it { should be_file }
  it { should be_mode 644 }
  its(:content) { should match(/AZURE_CLIENT_ID/) }
  its(:content) { should match(/AZURE_SECRET/) }
  its(:content) { should match(/AZURE_TENANT/) }
end

# bash
describe file('/etc/profile.d/bash_timeout.sh') do
  it { should be_file }
  it { should be_mode 644 }
end

# cloud.cfg
describe file('/etc/cloud/cloud.cfg') do
  it { should be_file }
  it { should contain '^# - update_hostname' }
end

# azurecli20 install
describe command('az -h') do
  its(:stdout) { should match /Group/ }
end
