# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

mesg n || true

AZURE_CLIENT_ID=<my azure client id>
AZURE_SECRET=<my azure secret key>
AZURE_TENANT=<my azure tenant id>
